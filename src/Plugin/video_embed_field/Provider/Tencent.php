<?php

namespace Drupal\video_embed_tencent\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * Tencent video provider.
 *
 * @VideoEmbedProvider(
 *   id = "tencent",
 *   title = @Translation("Tencent")
 * )
 */
class Tencent extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => 'tencent',
      '#url' => sprintf('//v.qq.com/iframe/player.html?vid=%s', $this->getVideoId() . '&'),
      '#query' => [
        'auto' => $autoplay,
        'tiny' => '0',
        'fmt' => 'fhd',
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return '//puui.qpic.cn/qqvideo_ori/0/' . $this->getVideoId() . '_360_204/0';
  }

  /**
   * Get the Tencent oEmbed data.
   *
   * @return array
   *   An array of data from the oEmbed endpoint.
   */
  protected function oEmbedData() {
    $video_id = $this->getVideoId();
    $file_content = file_get_contents('https://vv.video.qq.com/getinfo?vids=' . $video_id . '&otype=json&defaultfmt=fhd');
    // Remove QZOutputJson= and then closing semicolon.
    $embed_data = json_decode(substr($file_content, 13, -1), TRUE);
    return $embed_data->vl->vi[0];
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match('/^https?:\/\/(v\.)?qq.com\/x\/page\/(?<id>[0-9A-Za-z-._%&=]*)?\.html$/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->oEmbedData()->ti;
  }

}
