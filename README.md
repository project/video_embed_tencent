CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Current Maintainer: Elisabeth Escribano <elisabethec@gmail.com>

This module is a submodule from video_embed_field. Adds a provider to the 
list of remote video providers. The provider is Tencent.

REQUIREMENTS
------------

The requirement is that you are already using the module video_embed_field.

INSTALLATION
------------

This is like any other module. 

1. Add it to your project with composer 
"composer require drupal/video_embed_tencent".

2. Enable the module(s) and then you can configure it in the settings of your 
field type video_embed_field. In that field you can configure the providers 
and restrict them. Just make sure that if you have restriction you will need 
to adjust the configuration to be able to add remote videos from Tencent. 

CONFIGURATION
-------------

The configuration is under the field type video_embed_field. It appears in 
the list of allowed providers. If you have restricted the allowed providers 
you will need to specifically add the Tencent provider to the list of allowed 
providers in your field.
